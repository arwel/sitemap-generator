from urllib.request import urlopen
from urllib.error import URLError, HTTPError


def get_http_content(url: str) -> str:
    try:
        with urlopen(url) as response:
            return str(response.read())
    except (URLError, HTTPError) as e:
        print(e.reason, ' on url ', url)
        return ''
