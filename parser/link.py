from config import Config
from crawler import get_unique_site_links
from output.templates import render_link_item, render_links_wrapper
from datetime import date


def parse_links(config: Config) -> tuple:
    links = get_unique_site_links(config)
    print(len(links))
    step = 5000
    offset_start = 0
    offset_end = step

    date_str = date.today().strftime(config.date_format)
    sitemaps = []
    while(1):
        links_to_sitemap = links[offset_start:offset_end]
        if len(links_to_sitemap) < 1:
            break
        offset_start = offset_end
        offset_end = offset_end + step
        items = map(lambda link: render_link_item(link, date_str), links_to_sitemap)
        sitemaps.append(render_links_wrapper(''.join(items)))

    return tuple(sitemaps)
