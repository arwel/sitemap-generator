from shutil import rmtree
from os.path import join
from os import makedirs
from uuid import uuid4
from functools import partial
from multiprocessing import cpu_count, Pool
from config import Config
from output.templates import render_sitemap_item, render_sitemaps_wrapper
from datetime import date


def save_to_file(path: str, content: str):
    file_name = 'sitemap-' + str(uuid4()) + '.xml'
    with open(join(path, file_name), 'w') as file:
        file.write(content)

    return file_name


def regenerate_sitemap(config: Config, parts: tuple):
    rmtree(config.dest_path, ignore_errors=True)
    makedirs(config.dest_path, 0o755, True)
    names = tuple()

    date_str = date.today().strftime(config.date_format)

    with Pool(cpu_count()) as pool:
        names = tuple(pool.map(partial(save_to_file, config.dest_path), parts))
        pool.close()
        pool.join()

    main_sitemap_items = map(lambda name: render_sitemap_item(join(config.sitemap_url, name), date_str), names)
    with open(join(config.dest_path, config.main_sitemap_name), 'w') as file:
        file.write(render_sitemaps_wrapper(''.join(main_sitemap_items)))
