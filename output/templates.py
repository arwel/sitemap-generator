def render_links_wrapper(content: str) -> str:
    return '<?xml version="1.0" encoding="UTF-8"?>' + \
           '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' + \
           content + \
           '</urlset>'


def render_link_item(url: str, date: str) -> str:
    return '<url><loc>' + \
           url + \
           '</loc><lastmod>' + \
           date + \
           '</lastmod></url>'


def render_sitemaps_wrapper(content: str) -> str:
    return '<?xml version="1.0" encoding="UTF-8"?>' + \
           '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' + \
           content + \
           '</sitemapindex>'


def render_sitemap_item(url: str, date: str) -> str:
    return '<sitemap><loc>' + url + '</loc><lastmod>' + date + '</lastmod></sitemap>'
