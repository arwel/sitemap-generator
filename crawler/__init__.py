from multiprocessing import cpu_count, Pool, Lock
from util.http import get_http_content
from lxml.html import fromstring
from lxml.etree import HTMLParser
from functools import partial
from config import Config


def get_unique_site_links(config: Config) -> tuple:
    links = set()
    links_to_crawl = {config.origin, }
    while (1):
        parsed_links = crawl(tuple(links_to_crawl), config.origin)
        links = links.union(links_to_crawl)
        links_to_crawl = parsed_links.difference(links)
        if len(links_to_crawl) < 1:
            break
    return tuple(links)


def is_proper_link(link: str) -> bool:
    try:
        return len(link) > 1 and link.index('?') < 0
    except ValueError:
        return True


def crawl(urls: tuple, origin: str):
    with Pool(cpu_count()) as pool:
        pages_links = pool.map(partial(get_page_links, origin), urls)

        pool.close()
        pool.join()

        links_set = set()
        for links_list in pages_links:
            links_set = links_set.union(set(links_list))

        return links_set


def get_page_links(origin: str, url: str) -> set:
    parsed_urls = set()
    html = get_http_content(url)

    if len(html) < 10:
        return parsed_urls

    dom = fromstring(html, parser=HTMLParser())
    for a in tuple(dom.cssselect(f"a[href^=\"{origin}\"], a[href^=\"/\"]")):
        href = a.get('href').strip(' ')
        if is_proper_link(href):
            try:
                parsed_urls.add(origin + href if href[0] == '/' and href[1] != '/' else href)
            except IndexError:
                print('index error', href)

    return parsed_urls
