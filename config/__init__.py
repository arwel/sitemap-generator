from json import loads


class Config:
    _configured: bool = False
    __slots__ = (
        'origin',
        'robots_path',
        'dest_path',
        'mode',
        'exclude',
        'main_sitemap_name',
        'date_format',
        'sitemap_url'
    )

    def __init__(self):
        self.origin = ''
        self.robots_path = ''
        self.dest_path = ''
        self.mode = 'links'
        self.exclude = tuple()
        self.main_sitemap_name = 'sitemap.xml'
        self.date_format = '%Y-%m-%d'
        self.sitemap_url = ''

    def from_dict(self, config: dict):
        for key, val in config.items():
            setattr(self, key, val)
        Config._configured = True

    def from_file(self, path: str):
        with open(path) as file:
            content = file.read()
            self.from_dict(loads(content))
