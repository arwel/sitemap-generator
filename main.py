from output import regenerate_sitemap
from os import getcwd
from os.path import join
from sys import argv
from config import Config
from parser.link import parse_links


def get_config_path() -> str:
    config_path = join(getcwd(), '.sitemap-generator.cfg.json')
    if len(argv) > 1:
        config_path = str(argv[1]).strip(' ')

    return config_path


def main():
    config = Config()
    config.from_file(get_config_path())

    parser = {
        'links': parse_links
    }.get(config.mode)
    if parser is None:
        return
    sitemaps = parser(config)
    regenerate_sitemap(config, sitemaps)


if __name__ == '__main__':
    main()
